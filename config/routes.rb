Rails.application.routes.draw do
 
  root 'browse#index'
  resources :posts

  resources :browse, only: :index do
    get :authors, on: :collection
  end

end
