categories = []
%w{News Auto Music World IT}.each do |title|
  categories << Category.create(title: title)
end

authors = ["Joe", "Arch", "Jane", "Bob"]

50.times do
  category = categories.sample
  Post.create title: "Post about #{category.title}", category: category, publish_date: Time.now, author: authors.sample, text: "Text about #{category.title}!"
end