class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.references :category, index: true
      t.datetime :publish_date, index: true
      t.string :author, index: true
      t.text :text
      t.float :rating, index: true

      t.timestamps null: false
    end
    add_foreign_key :posts, :categories
  end
end
