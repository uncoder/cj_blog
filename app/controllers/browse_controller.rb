class BrowseController < ApplicationController
  POSTS_LIMIT = 10

  def index
    @posts = Post.includes(:category).order(rating: :desc).limit POSTS_LIMIT  
    filter_posts if params[:filter]
  end

  def authors
    @authors = Post.top_authors
  end

  private

  def filter_posts
    filter = params[:filter]
    @posts = @posts.where category_id: filter[:category]                unless filter[:category].blank?
    @posts = @posts.where author: filter[:author]                       unless filter[:author].blank?
    @posts = @posts.by_date filter[:publish_date]                       unless filter[:publish_date].blank?
    @posts = @posts.by_rating filter[:rating_from], filter[:rating_to]  unless filter[:rating_from].blank? and filter[:rating_to].blank?
  end

end
