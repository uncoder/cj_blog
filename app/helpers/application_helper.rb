module ApplicationHelper

  def modal_window options
    content_tag :div, id: options[:id], class: 'modal' do
      content_tag :div, class: 'modal-dialog' do
        content_tag :div, class: 'modal-content' do
          header = content_tag :div, class: 'modal-header' do
            title = content_tag :h4, class: 'modal-title' do
              options[:title]
            end
            button = content_tag :button, class: 'close', data: { dismiss: 'modal' },  type: 'button' do
              "×"
            end
            "#{button} #{title}".html_safe
          end
          body = content_tag :div, class: 'modal-body' do
            options[:body]
          end
          "#{header} #{body}".html_safe
        end
      end
    end
  end

end
