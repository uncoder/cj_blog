$('#post-form-modal').modal('hide');
$('#modal_holder').html "<%= escape_javascript(modal_window id: 'post-form-modal', title: 'Edit post', body: render('form')) %>";
$('#post-form-modal').modal('show');
$('.datetimepicker').datetimepicker()