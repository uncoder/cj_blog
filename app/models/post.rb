class Post < ActiveRecord::Base
  belongs_to :category

  validates :title, presence: true
  validates :category, presence: true
  validates :publish_date, presence: true
  validates :author, presence: true
  validates :text, presence: true

  before_create :set_random_rating

  scope :by_rating, -> (from, to) do
    to = 5 if to.blank?
    range = Range.new from.to_f, to.to_f
    where rating: range
  end

  scope :by_date, -> (date) do
    date_from = DateTime.parse date
    date_to = date_from + 1.day - 1.second
    range = date_from..date_to
    where publish_date: range
  end

  def set_random_rating
    self.rating = rand(0.0..5.0).round(2)
  end

  class << self
    def top_authors
      Post.order('average_rating desc').group(:author).average(:rating)
    end
  end
end
